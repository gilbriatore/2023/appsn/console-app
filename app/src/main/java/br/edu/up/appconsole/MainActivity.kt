package br.edu.up.appconsole

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import java.lang.reflect.Array

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var txt1: EditText
    lateinit var txt2: EditText
    lateinit var txtConsole: TextView
    lateinit var btnRadio: RadioButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        txt1 = findViewById(R.id.txt1)
        txt2 = findViewById(R.id.txt2)
        txtConsole = findViewById(R.id.txtConsole)
        btnRadio = findViewById(R.id.btnSomar)

        var btnProcessar: Button = findViewById(R.id.btnProcessar)
        btnProcessar.setOnClickListener(this)
    }

    fun somar(a: String, b: String)  {
        txtConsole.text =  (a.toInt() + b.toInt()).toString()
    }

    fun subtrair(a: String, b: String) {
        txtConsole.text =  (a.toInt() - b.toInt()).toString()
    }

    fun multiplicar(a: String, b: String) {
        txtConsole.text =  (a.toInt() * b.toInt()).toString()
    }

    fun dividir(a: String, b: String) {
        txtConsole.text =  (a.toInt() / b.toInt()).toString()
    }

    fun concatenar( a: String, b: String): String{
        return a + " " + b;
    }

    fun getTextoMaiorOuMenor(a: Int, b: Int): String{
        var texto: String
        if (a > b){
            texto = "A maior que B"
        } else {
            texto = "B maior do que A"
        }
        return texto
    }

    class Pessoa(val nome: String, val idade: Int)

    fun listaPessoas(nome: String, contador: Int): MutableList<Pessoa> {
        var lista = mutableListOf<Pessoa>()
        for(i in 1..contador){
            lista.add(Pessoa(nome, 23))
        }
        return lista
    }

    fun listar(contador: Int, texto: String): MutableList<String> {
        var lista = mutableListOf<String>()
        for(i in 1..contador) {
            lista.add(texto  + " " + i)
        }
        return lista
    }

    fun getStringPessoas(lista: MutableList<Pessoa>): String {
        var pessoas: String = ""
        for(p in lista){
            pessoas += p.nome + " " + p.idade + "\n"
        }
        return pessoas
    }


    fun getA(): String {
        return txt1.text.toString()
    }

    fun getB(): String {
        return txt2.text.toString()
    }

    fun getTemplateString(): String {
        return "Variável a = ${getA()}, variável b = ${txt2.text.toString()}"
    }

    override fun onClick(v: View?) {

        val a = getA()
        val b = getB()

        //txtConsole.text = concatenar(a, b)
        //txtConsole.text = getTemplateString()
        //txtConsole.text = somar(a, b).toString()

        //var lista = listar(a.toInt(), b)
        //txtConsole.text = lista.toString()

        //var listaPessoas = listaPessoas(a, b.toInt())
        //txtConsole.text = getStringPessoas(listaPessoas)

        when(btnRadio.id){
            R.id.btnSomar -> somar(a, b)
            R.id.btnSubtrair -> subtrair(a, b)
            R.id.btnMultiplicar -> multiplicar(a, b)
            R.id.btnDividir -> dividir(a,b)
        }
    }

    fun aoClicarNoRadioButton(view: android.view.View) {
        btnRadio = view as RadioButton;
    }
}